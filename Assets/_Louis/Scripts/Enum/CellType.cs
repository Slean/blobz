﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public enum CellType
{
    Productrice,
    Armory,
    Stockage,
    Broyeur,
    Passage
}
